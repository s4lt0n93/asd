﻿using System;
using System.Collections.Specialized;
using System.Runtime.Caching;

namespace TestMemoryCache
{


    class Program
    {
        private static readonly MemoryCache _cacheInstanced = new MemoryCache("AssetResolutionCache",
            config: new NameValueCollection
            {
                {"cacheMemoryLimitMegabytes", "1024"},
                {"physicalMemoryLimitPercentage", "50"},
                {"pollingInterval", TimeSpan.FromHours(1).ToString() }
            }, ignoreConfigSection: true);
        static void Main(string[] args)
        {
            var str = "hello";
            _cacheInstanced.Add(new CacheItem(str, str), new CacheItemPolicy { SlidingExpiration = TimeSpan.FromHours(1) });
            Console.WriteLine(_cacheInstanced.Get(str));
            Console.ReadLine();
        }
    }
}